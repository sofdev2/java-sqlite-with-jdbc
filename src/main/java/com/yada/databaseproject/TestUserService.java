/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.databaseproject;

import com.yada.databaseproject.model.User;
import com.yada.databaseproject.service.UserService;

/**
 *
 * @author ASUS
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user1","password");
        if(user!=null) {
            System.out.println("Welcome user : " + user.getName());
        }else {
            System.out.println("Error");
        }
    }
}
